# Slix – XeTeX Macros for Presentations

This project offers relatively simple
[plain TeX](https://en.wikibooks.org/wiki/LaTeX/Plain_TeX) macros for
creating PDF presentations in XeTeX. It grew out of my frustration
with the [Beamer](https://en.wikipedia.org/wiki/Beamer_(LaTeX))
package. As it is the case with many LaTeX packages, it works fine for
use cases that have been envisioned by its authors, but implementing
other designs often requires to study the complex internals of the
package and LaTeX itself.

# Prerequisites

[XeTeX](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=xetex)
is required. The package **won't** work with other TeX versions
without significant changes (except perhaps
[LuaTeX](http://www.luatex.org), but I have no experience with it). On
the other hand, XeTeX is a part of most modern TeX distributions such
as [TeX Live](https://www.tug.org/texlive/) or
[MacTeX](https://tug.org/mactex/), so it is very easy to get.

## Installation

The easiest way (on a Unix-like system, anyway) to install this
package is to clone it from [GitLab](https://gitlab.nic.cz/llhotka/slix) and then make symbolic links from a directory where TeX looks for
its input files, such as `~/texmf` or `~/Library/texmf`, to the
project directories `TeX` (and optionally `MetaPost`). For example:
``` bash
$ cd Projects
$ git clone git@gitlab.nic.cz:llhotka/slix.git
$ cd ~/texmf/tex
$ ln -s ~/Projects/slix/TeX slix
```
And similarly for MetaPost:
``` bash
$ cd ~/texmf/metapost
$ ln -s ~/Projects/slix/MetaPost slix
```

After that, don't forget to run
``` bash
$ texhash
```
after doing that.

## Presentation Styles

The package provides two presentation styles:

* `nicslix.tex` – CZ.NIC corporate style,

* `slix.tex` – bare style, no decorations (only automatic slide
  numbers), suitable e.g. for IETF meetings.

Feel free to create new styles, and consider contributing them to this project.

## Fonts

Unlike older versions of TeX, XeTeX uses OpenType fonts as standard system fonts.

The 2022 edition of the CZ.NIC visual style for presentations recommends the
freely available [Roboto](https://fonts.google.com/specimen/Roboto)
font family. Its [OpenType](https://en.wikipedia.org/wiki/OpenType)
version can be downloaded from the [CTAN
repository](http://tug.ctan.org/fonts/roboto/opentype/) and installed
according to operating system requirements.

The bare style uses some non-free fonts, which can be replaced by
other OpenType font families, provided they have
all the necessary typefaces (italic, boldface). To do this, just edit
the beginning of `slix.tex`. The bare style also uses the [Source Code
Pro](http://adobe-fonts.github.io/source-code-pro/) font as
the fixed-width (typewriter) font. It is freely available for download
from [GitHub](https://github.com/adobe-fonts/source-code-pro).

In both styles, mathematics is typeset using the
[AMS Euler](https://en.wikipedia.org/wiki/AMS_Euler) fonts that should
be present in any TeX installation.

## Creating Presentations

The easiest way to start with _slix_ is to copy `Makefile` from the
`example` directory to a new local directory, and put all TeX sources,
figures etc. there. The `Makefile` needs to be modified, usually it
suffices to set the `DOCUMENTS` variable – in the simplest case it
will contain the file name of the PDF presentation.

## Documentation

So far,
[user instructions](https://gitlab.labs.nic.cz/llhotka/slix/blob/master/example/texnicprez.pdf)
are only available in Czech, they can be found in the `example`
subdirectory. The source code
[texnicprez.tex](https://gitlab.labs.nic.cz/llhotka/slix/blob/master/example/texnicprez.tex)
can serve as a model for creating own presentations. If everything is
properly installed, it should be possible to regenerate this
presentation by running

```
$ make
```

in that directory.

Enjoy! Láďa Lhotka


