#! /usr/bin/env python
import sys
mp = open(sys.argv[1])
for rad in mp:
    if rad.startswith("%%BoundingBox:"):
        break
bbox = [int(dim) for dim in rad.split()[-4:]]
wd = bbox[2] - bbox[0]
ht = bbox[3] - bbox[1]
print(f"\\mppic{{{wd}bp}}{{{ht}bp}}{{{sys.argv[1]}}}")
